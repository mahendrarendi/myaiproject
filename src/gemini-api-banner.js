// import dotenv from 'dotenv';

// dotenv.config(); 

export function maybeShowApiKeyBanner() {
  const apiKey = process.env.VITE_API_KEY;

  if (!apiKey || apiKey === 'YOUR_GEMINI_API_KEY') {
    const banner = document.createElement('div');
    banner.style.position = 'fixed';
    banner.style.bottom = '0';
    banner.style.width = '100%';
    banner.style.backgroundColor = '#ffcc00';
    banner.style.color = '#000';
    banner.style.padding = '10px';
    banner.style.textAlign = 'center';
    banner.textContent = 'Please fill in your Gemini API key in the .env file.';
    document.body.appendChild(banner);
  }
}
