import { GoogleGenerativeAI, HarmBlockThreshold, HarmCategory } from "@google/generative-ai";
import MarkdownIt from 'markdown-it';
import { maybeShowApiKeyBanner } from './gemini-api-banner';

const API_KEY = import.meta.env.VITE_API_KEY;

document.addEventListener("DOMContentLoaded", () => {
  let form = document.querySelector('.chat-input');
  let promptInput = document.querySelector('input[name="prompt"]');
  let chatHistory = document.querySelector('.chat-history');
  let sidebarHistory = document.querySelector('.sidebar-history');
  let sidebar = document.getElementById('sidebar');
  let main = document.querySelector('main');
  let footer = document.querySelector('footer');

  // Load chat history from localStorage
  let chats = JSON.parse(localStorage.getItem('chats')) || [];

  // Display chat history in sidebar
  updateSidebarHistory();

  const genAI = new GoogleGenerativeAI(API_KEY);
  const model = genAI.getGenerativeModel({
    model: "gemini-1.5-pro",
    safetySettings: [
      {
        category: HarmCategory.HARM_CATEGORY_HARASSMENT,
        threshold: HarmBlockThreshold.BLOCK_ONLY_HIGH,
      },
    ],
  });

  let chat; // Define chat variable
  let md = new MarkdownIt(); // Initialize MarkdownIt instance

  form.addEventListener('submit', async (ev) => {
    ev.preventDefault();
    let userMessage = promptInput.value.trim(); // Trim whitespace
    if (userMessage === '') {
      userMessage = promptInput.placeholder; // Use placeholder if input is empty
    }
    addMessageToChat('user', md.render(userMessage));
    promptInput.value = '';

    let statusElement = document.createElement('div');
    statusElement.className = 'status';
    statusElement.textContent = 'Thinking...';
    chatHistory.appendChild(statusElement);
    chatHistory.scrollTop = chatHistory.scrollHeight;

    try {
      // Initialize chat if not already initialized
      if (!chat) {
        chat = model.startChat({
          history: [],
          generationConfig: {
            maxOutputTokens: 7000000
          }
        });
      }

      const result = await chat.sendMessageStream(userMessage);

      let buffer = [];
      for await (let response of result.stream) {
        buffer.push(response.text());
      }
      chatHistory.removeChild(statusElement);
      let botMessage = buffer.join('');
      addMessageToChat('bot', md.render(botMessage));

      // Save chat to localStorage with a summary
      let summary = userMessage.length > 50 ? userMessage.slice(0, 50) + '...' : userMessage;
      chats.push({ summary: summary, messages: [{ sender: 'user', text: userMessage }, { sender: 'bot', text: botMessage }] });
      localStorage.setItem('chats', JSON.stringify(chats));

      // Update sidebar with latest chat
      updateSidebarHistory();
    } catch (e) {
      chatHistory.removeChild(statusElement);
      addMessageToChat('bot', md.render('Error: ' + e.message));
    }
  });

  function addMessageToChat(sender, message) {
    let messageElement = document.createElement('div');
    messageElement.className = `message ${sender}`;
    messageElement.innerHTML = message; // Directly set HTML content
    chatHistory.appendChild(messageElement);
    chatHistory.scrollTop = chatHistory.scrollHeight; // Scroll to the bottom
  }

  function updateSidebarHistory() {
    sidebarHistory.innerHTML = ''; // Clear existing sidebar history

    // Re-render chat history in sidebar
    chats.forEach((chat, index) => {
      let listItem = document.createElement('div');
      listItem.classList.add('sidebar-history-item');

      let summaryText = document.createElement('span');
      summaryText.textContent = chat.summary;
      summaryText.addEventListener('click', () => {
        console.log(`Clicked on chat ${index}`);
        loadChatHistory(index);
      });

      let deleteButton = document.createElement('button');
      deleteButton.classList.add('delete-button');
      deleteButton.innerHTML = `
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2">
          <polyline points="3 6 5 6 21 6"></polyline>
          <path d="M19 6l-2 14H7L5 6"></path>
          <path d="M10 11v6"></path>
          <path d="M14 11v6"></path>
          <path d="M4 6V4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v2"></path>
        </svg>
      `;
      deleteButton.addEventListener('click', (e) => {
        e.stopPropagation(); // Prevent click event from bubbling up
        console.log(`Deleted chat ${index}`);
        deleteChatHistory(index);
      });

      listItem.appendChild(summaryText);
      listItem.appendChild(deleteButton);
      sidebarHistory.appendChild(listItem);
    });
  }

  function loadChatHistory(index) {
    console.log(`Loading chat history for chat ${index}`);
    chatHistory.innerHTML = ''; // Clear existing chat history
    chats[index].messages.forEach(chat => {
      addMessageToChat(chat.sender, md.render(chat.text));
    });
  }

  function deleteChatHistory(index) {
    console.log(`Deleting chat history for chat ${index}`);
    chats.splice(index, 1); // Remove chat from array
    localStorage.setItem('chats', JSON.stringify(chats)); // Update localStorage
    updateSidebarHistory(); // Update sidebar display
    chatHistory.innerHTML = ''; // Clear chat history display
  }

  // Select element sidebar dan toggle button
  const toggleSidebarBtn = document.getElementById('toggleSidebar');

  // Tambahkan event listener untuk toggle sidebar
  toggleSidebarBtn.addEventListener('click', () => {
    sidebar.classList.toggle('active'); // Toggle class 'active' pada sidebar
    main.classList.toggle('shifted'); // Sesuaikan margin utama saat sidebar aktif
    footer.classList.toggle('shifted'); // Sesuaikan margin footer saat sidebar aktif
    console.log("Sidebar toggled");
  });

  maybeShowApiKeyBanner(API_KEY);
});
