document.addEventListener('DOMContentLoaded', function() {
    const sidebarToggle = document.querySelector('.sidebar-toggle');
    const sidebar = document.querySelector('.sidebar');
    const main = document.querySelector('main');
  
    sidebarToggle.addEventListener('click', function() {
      sidebar.classList.toggle('active');
      main.classList.toggle('active');
    });
  
    chatHistory.forEach(entry => {
      const li = document.createElement('li');
      li.classList.add(entry.type);
      li.textContent = entry.message;
      sidebarHistory.appendChild(li);
    });
  
    // Example of dynamically adding chat messages to main chat history
    const chatHistoryContainer = document.querySelector('.chat-history');
    chatHistory.forEach(entry => {
      const messageDiv = document.createElement('div');
      messageDiv.classList.add('message', entry.type);
      messageDiv.textContent = entry.message;
      chatHistoryContainer.appendChild(messageDiv);
    });
  
  });
  
  // main.js

const toggleButton = document.getElementById('toggleSidebar');
const sidebar = document.getElementById('sidebar');

toggleButton.addEventListener('click', function() {
  sidebar.classList.toggle('active');
});
